package ru.tsc.anaumova.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.anaumova.tm.api.model.IWBS;
import ru.tsc.anaumova.tm.enumerated.Status;
import ru.tsc.anaumova.tm.util.DateUtil;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public final class Task extends AbstractUserOwnedModel implements IWBS {

    @NotNull
    private String name = "";

    @NotNull
    private String description = "";

    @NotNull
    private Status status = Status.NOT_STARTED;

    @Nullable
    private String projectId;

    @NotNull
    private Date created = new Date();

    @Nullable
    private Date dateBegin;

    @Nullable
    private Date dateEnd;

    public Task(
            @NotNull final String name,
            @NotNull final Status status,
            @NotNull final Date dateBegin
    ) {
        this.name = name;
        this.status = status;
        this.dateBegin = dateBegin;
    }

    @Override
    public String toString() {
        return name + " ; " + description + " ; " + Status.toName(status) +
                "; Created - " + DateUtil.toString(created) +
                "; Begin - " + DateUtil.toString(dateBegin) +
                "; End - " + DateUtil.toString(dateEnd) + ";";
    }

}