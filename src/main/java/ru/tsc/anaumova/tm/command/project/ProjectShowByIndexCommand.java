package ru.tsc.anaumova.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.anaumova.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.anaumova.tm.model.Project;
import ru.tsc.anaumova.tm.util.TerminalUtil;

public final class ProjectShowByIndexCommand extends AbstractProjectCommand {

    @NotNull
    public static final String NAME = "project-show-by-index";

    @NotNull
    public static final String DESCRIPTION = "Show project by index.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECT BY INDEX]");
        System.out.println("[ENTER INDEX:]");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final String userId = getUserId();
        @Nullable final Project project = serviceLocator.getProjectService().findOneByIndex(userId, index);
        if (project == null) throw new ProjectNotFoundException();
        showProject(project);
    }

}