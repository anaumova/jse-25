package ru.tsc.anaumova.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.anaumova.tm.enumerated.Role;
import ru.tsc.anaumova.tm.util.TerminalUtil;

public final class UserRegistryCommand extends AbstractUserCommand {

    @NotNull
    public static final String NAME = "user-registry";

    @NotNull
    public static final String DESCRIPTION = "Registry new user.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[ENTER LOGIN:]");
        @NotNull String login = TerminalUtil.nextLine();
        System.out.println("[ENTER PASSWORD:]");
        @NotNull String password = TerminalUtil.nextLine();
        System.out.println("[ENTER EMAIL:]");
        @NotNull String email = TerminalUtil.nextLine();
        serviceLocator.getAuthService().registry(login, password, email);
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

}
