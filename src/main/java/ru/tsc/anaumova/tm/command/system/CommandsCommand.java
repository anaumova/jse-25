package ru.tsc.anaumova.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.tsc.anaumova.tm.command.AbstractCommand;

import java.util.Collection;

public final class CommandsCommand extends AbstractSystemCommand {

    @NotNull
    public static final String NAME = "commands";

    @NotNull
    public static final String ARGUMENT = "-cmd";

    @NotNull
    public static final String DESCRIPTION = "Show command list.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[COMMANDS]");
        @NotNull final Collection<AbstractCommand> commands = serviceLocator.getCommandService().getTerminalCommands();
        for (final AbstractCommand command : commands) {
            @NotNull final String name = command.getName();
            if (name.isEmpty()) continue;
            System.out.println(name);
        }
    }

}