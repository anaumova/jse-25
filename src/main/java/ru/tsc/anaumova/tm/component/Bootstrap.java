package ru.tsc.anaumova.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.tsc.anaumova.tm.api.repository.ICommandRepository;
import ru.tsc.anaumova.tm.api.repository.IProjectRepository;
import ru.tsc.anaumova.tm.api.repository.ITaskRepository;
import ru.tsc.anaumova.tm.api.repository.IUserRepository;
import ru.tsc.anaumova.tm.api.service.*;
import ru.tsc.anaumova.tm.command.AbstractCommand;
import ru.tsc.anaumova.tm.command.system.*;
import ru.tsc.anaumova.tm.enumerated.Role;
import ru.tsc.anaumova.tm.enumerated.Status;
import ru.tsc.anaumova.tm.exception.system.UnknownArgumentException;
import ru.tsc.anaumova.tm.exception.system.UnknownCommandException;
import ru.tsc.anaumova.tm.model.User;
import ru.tsc.anaumova.tm.repository.CommandRepository;
import ru.tsc.anaumova.tm.repository.ProjectRepository;
import ru.tsc.anaumova.tm.repository.TaskRepository;
import ru.tsc.anaumova.tm.repository.UserRepository;
import ru.tsc.anaumova.tm.service.*;
import ru.tsc.anaumova.tm.util.TerminalUtil;

import java.lang.reflect.Modifier;
import java.util.Set;

public class Bootstrap implements IServiceLocator {

    @NotNull
    private static final String PACKAGE_COMMANDS = "ru.tsc.anaumova.tm.command";

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @Getter
    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @Getter
    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @Getter
    @NotNull
    private final IUserService userService = new UserService(userRepository, projectRepository, taskRepository, propertyService);

    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(propertyService, userService);

    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    {
        @NotNull final Reflections reflections = new Reflections(PACKAGE_COMMANDS);
        @NotNull final Set<Class<? extends AbstractCommand>> classes =
                reflections.getSubTypesOf(AbstractCommand.class);
        for (@NotNull final Class<? extends AbstractCommand> clazz : classes) registry(clazz);
    }

    @SneakyThrows
    private void registry(@NotNull final Class<? extends AbstractCommand> clazz) {
        if (Modifier.isAbstract(clazz.getModifiers())) return;
        if (!AbstractCommand.class.isAssignableFrom(clazz)) return;
        final AbstractCommand command = clazz.newInstance();
        registry(command);
    }

    private void initDemoData() {
        User usualUser = userService.create("user", "user", "user@user.ru");
        User adminUser = userService.create("admin", "admin", Role.ADMIN);
        projectService.create(usualUser.getId(), "Project C", "15.11.2019").setStatus(Status.IN_PROGRESS);
        projectService.create(usualUser.getId(), "Project A", "13.05.2021").setStatus(Status.COMPLETED);
        projectService.create(usualUser.getId(), "Project D", "02.02.2020").setStatus(Status.IN_PROGRESS);
        projectService.create(adminUser.getId(), "Project B", "01.02.2019").setStatus(Status.NOT_STARTED);
        taskService.create(usualUser.getId(), "Task F", "05.10.2018").setStatus(Status.IN_PROGRESS);
        taskService.create(adminUser.getId(), "Task E", "11.04.2021").setStatus(Status.NOT_STARTED);
        taskService.create(adminUser.getId(), "Task G", "02.02.2021").setStatus(Status.NOT_STARTED);
    }

    private void initLogger() {
        loggerService.info("** WELCOME TO TASK-MANAGER **");
        Runtime.getRuntime().addShutdownHook(
                new Thread(() -> loggerService.info("** TASK-MANAGER IS SHUTTING DOWN **"))
        );
    }

    public void run(@Nullable final String[] args) {
        if (processArgument(args)) new ExitCommand().execute();
        initLogger();
        initDemoData();
        while (true) {
            try {
                System.out.println("ENTER COMMAND:");
                @NotNull final String command = TerminalUtil.nextLine();
                processCommand(command);
                loggerService.command(command);
            } catch (final Exception e) {
                loggerService.error(e);
            }
        }
    }

    private void processCommand(@NotNull final String command) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new UnknownCommandException(command);
        authService.checkRoles(abstractCommand.getRoles());
        abstractCommand.execute();
    }

    private void processArgument(@Nullable final String arg) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByArgument(arg);
        if (abstractCommand == null) throw new UnknownArgumentException(arg);
        abstractCommand.execute();
    }

    private boolean processArgument(@Nullable final String[] args) {
        if (args == null || args.length == 0) return false;
        @Nullable final String arg = args[0];
        processArgument(arg);
        return true;
    }

    private void registry(@NotNull final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

}